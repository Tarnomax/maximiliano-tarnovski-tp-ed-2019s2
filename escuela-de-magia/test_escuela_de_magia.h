#ifndef TEST_ESCUELA_DE_MAGIA_H_INCLUDED
#define TEST_ESCUELA_DE_MAGIA_H_INCLUDED

#include <vector>
#include "escuela_magia.h"
#include "../common/Test.h"


/***************************** Tests de Escuela de magia *****************************/

TEST(test_escuela_no_empty, {
     EscuelaDeMagia escuela =  fundarEscuela();
     ASSERT_EQ(escuela == NULL , false);
     } )

 TEST(test_escuela_empty, {
    EscuelaDeMagia escuela =  fundarEscuela();
    ASSERT_EQ(estaVacia(escuela)== true, true);
    })


TEST(test_escuela_harry, {
    EscuelaDeMagia escuela =  fundarEscuela();
    registrar("Harry",escuela);
    ASSERT_EQ(estaVacia(escuela)== true, false);
    })


TEST (test_resgistrar_varios , {
    EscuelaDeMagia escuela = fundarEscuela();
    registrar("Harry",escuela);
    registrar("Merlo",escuela);
    ASSERT_EQ( (magos(escuela).empty()) == false, true);
      })


TEST (test_size_escuela, {
    EscuelaDeMagia escuela = fundarEscuela();
    registrar("Harry",escuela);
    registrar("hola",escuela);
    registrar("hola",escuela);
    registrar("hola",escuela);
    vector<string> m1 = magos(escuela);
    ASSERT_EQ( m1.size() == 2, true);
      })


TEST (test_hechizos_de, {
    EscuelaDeMagia escuela = fundarEscuela();
    registrar("Harry",escuela);
    registrar("Hermaione",escuela);
    Hechizo h1 = crearHechizo("Kaio-ken", 1000);
    Hechizo h2 = crearHechizo("kame-kame-ha",2000);
    enseniar(h1,"Hermaione",escuela);
    enseniar(h2,"Hermaione",escuela);
    enseniar(h2,"Harry",escuela);
    Set hechizos = hechizosDe("Hermaione",escuela);
    ASSERT_EQ( sizeS(hechizos) ==  2, true);
      })


TEST (test_egresar_crack, {
    EscuelaDeMagia escuela = fundarEscuela();
    registrar("Harry",escuela);
    registrar("Hermaione",escuela);
    Hechizo h1 = crearHechizo("Kaio-ken", 1000);
    Hechizo h2 = crearHechizo("kame-kame-ha",2000);
    enseniar(h1,"Hermaione",escuela);
    enseniar(h2,"Hermaione",escuela);
    enseniar(h1,"Harry",escuela);
    Mago m1 = unEgresado(escuela);
    ASSERT_EQ(nombreMago(m1)=="Hermaione", true);

      })


TEST (test_quitar_egresado, {
    EscuelaDeMagia escuela = fundarEscuela();
    registrar("Harry",escuela);
    registrar("Hermaione",escuela);
    Hechizo h1 = crearHechizo("Kaio-ken", 1000);
    Hechizo h2 = crearHechizo("kame-kame-ha",2000);
    enseniar(h1,"Hermaione",escuela);
    enseniar(h2,"Harry",escuela);
    enseniar(h1,"Harry",escuela);
    quitarEgresado(escuela);
    vector<string> m1 = magos(escuela);
    ASSERT_EQ(m1.size()==1 , true);

})

TEST (test_queda_un_solo_mago,{
    EscuelaDeMagia escuela = fundarEscuela();
    registrar("Harry",escuela);
    registrar("Hermaione",escuela);
    Hechizo h1 = crearHechizo("Kaio-ken", 1000);
    Hechizo h2 = crearHechizo("kame-kame-ha",2000);
    enseniar(h1,"Hermaione",escuela);
    enseniar(h2,"Harry",escuela);
    enseniar(h1,"Harry",escuela);
    unEgresado(escuela);
    quitarEgresado(escuela);
    Mago mago2 = unEgresado(escuela);
    ASSERT_EQ( nombreMago(mago2) == "Hermaione" , true); /// despues de egresar a Harry, solo queda Hermaione

})


TEST ( test_leFalta_apreder, {
       EscuelaDeMagia escuela = fundarEscuela();
    registrar("Jose",escuela);
    registrar("Laura",escuela);
    registrar("Hermo",escuela);
    Hechizo h1 = crearHechizo("Kaio-ken", 1000);
    Hechizo h2 = crearHechizo("kame-kame-ha",2000);
    Hechizo h3 = crearHechizo("kame",2000);
    enseniar(h1,"Hermo",escuela);
    enseniar(h2,"Jose",escuela);
    enseniar(h3,"Laura",escuela);
    enseniar(h2,"Laura",escuela);
    ASSERT_EQ(leFaltanAprender("Jose",escuela) == 2, true); /// Hay 3 hechizo en la escuela pero el solo
    /// aprendio 1.

})


void correr_test_escuela_de_magia () {
    test_escuela_no_empty();
    test_escuela_empty();
    test_escuela_harry();
    test_size_escuela();
    test_resgistrar_varios();
    test_hechizos_de();
    test_egresar_crack();
    test_quitar_egresado();
    test_queda_un_solo_mago();
    test_leFalta_apreder();

}



#endif // TEST_ESCUELA_DE_MAGIA_H_INCLUDED
