#include <iostream>
#include <cstdlib>
#include <vector>
#include "../hechizo/hechizo.h"
#include "../mago/mago.h"
#include "../set/set.h"
#include "../map/map.h"
#include "../maxheap/maxheap.h"
#include "escuela_magia.h"
#include "../common/Common.h"
#include "test_escuela_de_magia.h"
#include <string.h>


using namespace std;

/// Proposito: Retorna todos los hechizos aprendidos por los magos.
/// Eficiencia:  O  (m.(h^2))
Set hechizosAprendidos(EscuelaDeMagia m) {
   Set hechizosE = emptyS();
   vector<string> magosE = magos(m); ///O(m)
   int cantMagos = magosE.size(); /// O(m)

   for (int i=0 ; i < cantMagos ; i++){
     hechizosE = unionS(hechizosE,hechizosDe(magosE[i],m));/// O m.((h^2))
   }
   return hechizosE;
}

/// Proposito: Indica si existe un mago que sabe todos los hechizos ense�ados por la escuela.
/// Eficiencia: O  (m.(h^2)) -- Si la hice bien esta funcion cuesta   O (2.log m)
bool hayUnExperto(EscuelaDeMagia m) {

   return (leFaltanAprender(nombreMago(unEgresado(m)),m) == 0 );
}

/// Proposito: Devuelve una maxheap con los magos que saben todos los hechizos dados por la escuela, quit�ndolos de la escuela.
/// Eficiencia: /// O  (m .(2.log m))
MaxHeap egresarExpertos(EscuelaDeMagia m) {
    MaxHeap heap = emptyH();

    while (hayUnExperto(m)){ /// O  m .(2.log m)
        insertH(unEgresado(m),heap);
        quitarEgresado(m);
    }
    return heap;
}




///FUNCION AUXILIAR PARA MAIN.
string returnUser (bool b){

    if(b){
        return "Si hay, es ";
    }else {
        return "No hay  :(";
    }
}

int main()
{
    correr_test_escuela_de_magia();


    /// Lo que continua debajo no tiene relacion con los TESTS.
    EscuelaDeMagia escuela = fundarEscuela();
    registrar("Jose",escuela);
    registrar("Laura",escuela);
    registrar("Hermo",escuela);
    Hechizo h1 = crearHechizo("Kaio-ken", 1000);
    Hechizo h2 = crearHechizo("kame-kame-ha",2000);
    Hechizo h3 = crearHechizo("kame",2000);
    enseniar(h1,"Hermo",escuela);
    enseniar(h2,"Jose",escuela);
    enseniar(h3,"Laura",escuela);
    enseniar(h2,"Laura",escuela);

    cout << "\nLa cantidad de hechizos que hay en la escuela es " << sizeS(hechizosAprendidos(escuela))<<endl;


    cout << "\nHay algun experto en la escuela? " << returnUser(hayUnExperto(escuela))<<  endl;


    enseniar(h1,"Laura",escuela); /// La ense�o h1 a Laura para que egrese.

    cout << "\nY ahora... Hay algun experto en la escuela? " << returnUser(hayUnExperto(escuela))
    <<nombreMago(unEgresado(escuela)) <<endl;

    egresarExpertos(escuela); /// Egreso a Laura.


    enseniar(h1,"Jose",escuela); /// Lo hago el nuevo crack de la escuela a Jose.


    cout <<  "\nSi sacamos a los expertos de la escuela solo quedan " <<
    magos(escuela).size() << /// Deben quedar 2 poruqe egrese a Laura.
    ". Ahora el que mas sabe es " << nombreMago(unEgresado(escuela))<< "."<< endl;


    cout << "\nAhora que egreso un mago. Hay otro experto en la escuela? " << returnUser(hayUnExperto(escuela))<<  endl;
    /// No hay porque le falta aprender h3 a Jose.

    return 0;
}


