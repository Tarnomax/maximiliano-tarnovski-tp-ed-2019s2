#include "escuela_magia.h"
#include "../common/Common.h"

struct EscuelaDeMagiaSt {
   MaxHeap heapMagos;
   Map mapMagos;
   Set hechizos;
};

/// Prop�sito: Devuelve una escuela vac�a.
/// O(1)
EscuelaDeMagia fundarEscuela() {
   EscuelaDeMagia escuela = new EscuelaDeMagiaSt;
   escuela->heapMagos = emptyH();
   escuela->mapMagos = emptyM();
   escuela->hechizos = emptyS();

   return escuela;
}

/// Prop�sito: Indica si la escuela est� vac�a.
/// O(1)
bool estaVacia(EscuelaDeMagia m) {
   return isEmptyH(m->heapMagos);
}


/// Proposito : devuelve True si el mago pertenece en la escuela.
/// O(log n), n es la cantidad de claves
bool perteneceAEscuela (string nombre, Map m){
    return (lookupM(nombre,m) != NULL);

}


/// Prop�sito: Incorpora un mago a la escuela (si ya existe no hace nada).
/// O(log m)  -- assocM e insertH son log m.
void registrar(string nombre, EscuelaDeMagia m) {

    if (!(perteneceAEscuela(nombre,m->mapMagos))){ /// llamo esta funcion por si el user inserta dos
        assocM(nombre,crearMago(nombre),m->mapMagos);
        insertH(crearMago(nombre),m->heapMagos);
    }
}

/// Prop�sito: Devuelve los nombres de los magos registrados en la escuela.
/// O(m) -- precio de domM.

vector<string> magos(EscuelaDeMagia m) {
   return domM(m->mapMagos);
}

/// Prop�sito: Devuelve los hechizos que conoce un mago dado.
/// Precondici�n: Existe un mago con dicho nombre.
/// O(log m) -- precio de lookupM

Set hechizosDe(string nombre, EscuelaDeMagia m) {
   return hechizosMago(lookupM(nombre,m->mapMagos));
}



/// Prop�sito: Dado un mago, indica la cantidad de hechizos que la escuela ha dado y �l no sabe.
/// Precondici�n: Existe un mago con dicho nombre.
/// O(log m) -- precio de hechizosDe

int leFaltanAprender(string nombre, EscuelaDeMagia m) {
   return sizeS(m->hechizos) - (sizeS(hechizosDe(nombre,m)));
}

/// Prop�sito: Devuelve el mago que m�s hechizos sabe.
/// Precondici�n: La escuela no est� vac�a.
/// O(log m) -- En realidad �sta funcion vale O(1)- en caso de que �sto sea lo que se pida.
Mago unEgresado(EscuelaDeMagia m) {
    return maxH(m->heapMagos);
    }

/// Prop�sito: Devuelve la escuela sin el mago que m�s sabe.
/// Precondici�n: La escuela no est� vac�a.
/// O(log m) - deleteMax y deleteM valen log n.
void quitarEgresado(EscuelaDeMagia m) {
   Mago magoC = maxH(m->heapMagos);
   deleteMax(m->heapMagos);
   deleteM(nombreMago(magoC),m->mapMagos);
}


/// Proposito = Agrega a la escuela el mago ya con el hechizo aprendido.
/// O (m . (log m))
void heapNuevo (EscuelaDeMagia e, Mago m) {
    MaxHeap heapO = e->heapMagos;
    e->heapMagos = emptyH();

    while (sizeH(heapO) != 0) { /// O (m . (log m))
        if (nombreMago(m) != nombreMago(maxH(heapO))){ /// O(1)
           insertH(maxH(heapO),e->heapMagos);  /// log (m)
           deleteMax(heapO);  /// log (m)

        }  else {
            insertH(m,e->heapMagos); /// log (m)
            deleteMax(heapO); /// log (m)
          }
    }
}
/// Prop�sito: Ense�a un hechizo a un mago existente, y si el hechizo no existe en la escuela es incorporado a la misma.
/// O(m . log m + log h)
void enseniar(Hechizo h, string nombre, EscuelaDeMagia m) {
    addS(h,m->hechizos);  ///O(h)
    Mago mago1 = lookupM(nombre,m->mapMagos); /// log n
    aprenderHechizo(h,mago1);  /// O(h)
    assocM(nombre,mago1, m->mapMagos); /// log (n)
    heapNuevo(m,mago1); /// O (m . (log m))
}



/// Prop�sito: Libera toda la memoria creada por la escuela (incluye magos, pero no hechizos).
void destruirEscuela(EscuelaDeMagia m) {
    destroyH(m->heapMagos);
    destroyM(m->mapMagos);
    delete m;
}


