#include "set.h"
#include "../common/Common.h"

struct Node {
   Hechizo elem; // el elemento que este nodo almacena
   Node* next; // siguiente nodo de la cadena de punteros
};

struct SetSt {
   int size; // cantidad de elementos del conjunto
   Node* first  ; // puntero al primer elemento
};

/**
  Invariantes de representacion:
    - size es la cantidad de nodos
    - no hay nodos con hechizos repetidos
**/

/// Proposito: retorna un conjunto de hechizos vacio
/// Costo: O(1)
Set emptyS() {
   Set set1 = new SetSt;
   set1 -> size = 0;
   set1 -> first = NULL;

   return set1;
}

/// Proposito: retorna la cantidad de hechizos
/// Costo: O(1)
int sizeS(Set s) {
   return s->size;
}

/// Proposito: indica si el hechizo pertenece al conjunto
/// Costo: O(h), h = cantidad de hechizos
bool belongsS(Hechizo h, Set s) {
   Node* nodo = s->first;
   while (nodo != NULL){
        if (mismoHechizo(h, nodo->elem)){
            return true;
        }
        else {
            nodo = nodo->next;
        }
   }
    return false;
}


/// Proposito: agrega un hechizo al conjunto
/// Costo: O(h), h = cantidad de hechizos
void addS(Hechizo h, Set s){
    int sizess = sizeS(s);
    Node* nodo = s->first;
    if  (!(belongsS(h,s))){
        s->first = new Node;
        s->first->elem = h;
        s->first->next = nodo;
        s->size = (sizess+1);
    }
}

/// Proposito: borra un hechizo del conjunto (si no existe no hace nada)
/// Costo: O(h), h = cantidad de hechizos
void removeS(Hechizo h, Set s) {
    int sizess = s->size;
    if (s->first != NULL){
        Node* aux = s->first;
        Node* anterior = NULL;

        while ((aux != NULL) && (aux->elem != h)){
            anterior = aux;
            aux = aux->next;
        }
        if (aux == NULL){ // El hechizo no existe en el set
            cout <<"El elemento no esta"<<endl;
        }
        else if (anterior == NULL) {//El primer hechizo es el que tengo que borrar porque no entro en el while y aux no es == NULL.
            s->first = s->first->next;
            sizess--;
            s->size = sizess;
            delete aux;
        }
        else {
            anterior->next  = aux ->next;
            sizess--;
            s->size = sizess;
            delete aux;
        }
    }
}
/// Proposito: borra toda la memoria consumida por el conjunto (pero no la de los hechizos)
/// Costo: O(n)
void destroyS(Set s) {

    if (s->first != NULL){
       Node* nodo1 = s->first;
       while (nodo1 != NULL){
            Node* nodo2 = nodo1->next;
            delete nodo1;
            nodo1= nodo2;
       }
    }
}




/// Proposito: retorna un nuevo conjunto que es la union entre ambos (no modifica estos conjuntos)
/// Costo: O(h^2), h = cantidad de hechizos
Set unionS(Set s1, Set s2) {
   Set set1 = emptyS();
   Node* nodo1 = s1->first;
   Node* nodo2 = s2->first;
   while ((nodo1 != NULL ) || (nodo2 != NULL)){
        if (nodo1 != NULL ){
            addS(nodo1->elem, set1);
            nodo1 = nodo1->next;
        }
        if (nodo2 != NULL){
            addS(nodo2->elem, set1);
            nodo2 = nodo2->next;
        }
   }
   return set1;
}




